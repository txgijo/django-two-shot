from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout     ### SignUpForm
from django.contrib.auth.models import User                     ### SignUpForm
from accounts.forms import LoginForm, SignUpForm                ### SignupForm


# Create your views here.
######  http://localhost:8000/accounts/login/


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']

            if password == password_confirmation:       ##### Create a new user with those values
                user = User.objects.create_user(        ##### and save it to a variable
                    username,
                    password=password,                                 
                )
                login(request, user)                    ##### Login the user with the user you just created
                return redirect("home")
        else:
            form.add_error("password", "Passwords do not match")
    else:
        form = SignUpForm()
    context = {
      "form": form,
    }
    return render(request, "accounts/signup.html", context)

##### http://localhost:8000/accounts/signup/


def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(                                        ##### only checks for existing user in DB
                request, 
                username=username, 
                password=password
            )
            if user:                                                    ##### actual user login
                login(request, user)                                    ##### 
                return redirect("home")
    else:
        form = LoginForm()# Return an 'invalid login' error message.
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)

##### http://localhost:8000/accounts/login/


def user_logout(request):
    logout(request)                         ### built in logout function
    return redirect("login")