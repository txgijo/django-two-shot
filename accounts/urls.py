from django.urls import path
from accounts.views import signup, user_login, user_logout


urlpatterns = [
    #path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
    #path("items/create/", todo_item_create, name="todo_item_create"),
    #path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    #path("<int:id>/edit/",edit_todolist, name="todo_list_update"),
    #path("create/", create_list, name="todo_list_create"),
    #path("<int:id>/",show_list, name="todo_list_detail"),
    path("signup/", signup, name="signup"),
    path("logout/", user_logout, name="logout"),
    path("login/", user_login, name="login"),
]