from django.db import models
from django.conf import settings


# Create your models here.
class Receipt(models.Model):    
    vendor = models.CharField(max_length=200)         
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField(null=True)

    purchaser = models.ForeignKey(                             ##### LOOKUP how this works
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    category = models.ForeignKey(                             ##### LOOKUP how this works
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    account = models.ForeignKey(                             ##### LOOKUP how this works
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null = True,                ##### optional, False by default
    )

                                        ##### for humans


class Account(models.Model):
    name = models.CharField(max_length=100)  
    number = models.CharField(max_length=20)            ##### only characters NOT numbers
    owner = models.ForeignKey(                              ##### LOOKUP how this works
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)           
    owner = models.ForeignKey(                              ##### LOOKUP how this works
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
