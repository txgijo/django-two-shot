from django.urls import path
from receipts.views import receipt_list, create_receipt, list_accounts, list_categories, create_category, create_account


urlpatterns = [
    #path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
    #path("items/create/", todo_item_create, name="todo_item_create"),
    #path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    #path("<int:id>/edit/",edit_todolist, name="todo_list_update"),
    #path("<int:id>/",show_list, name="todo_list_detail"),

    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
    path("categories/", list_categories, name="list_categories"),
    path("accounts/", list_accounts, name="list_accounts"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_list, name="home"),
]