from django.shortcuts import render, redirect, get_object_or_404
from receipts.models import Receipt, Account, ExpenseCategory
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, CreateCategoryForm, CreateAccountForm


# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts, 
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():                         ### We should use the form to validate the values
            receipt = form.save(False)                                                       
            receipt.purchaser = request.user
            receipt.save()                          ### and save them to the DB
            return redirect("home")                 ### to another page and leave the function
    else:
        form  = CreateReceiptForm()                 ### Create an instance of the Django model form class
    context = {                                     ### Put the form in the context
        "form": form,
        }
                                                    ### Render the HTML template with the form
    return render(request, "receipts/create.html", context)

##### http://localhost:8000/receipts/create/


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateCategoryForm(request.POST)
        if form.is_valid():                         ### We should use the form to validate the values
            category = form.save(False)                                                       
            category.owner = request.user
            category.save()                          ### and save them to the DB
            return redirect("list_categories")                 ### to another page and leave the function
    else:
        form  = CreateCategoryForm()                ### Create an instance of the Django model form class
    context = {                                     ### Put the form in the context
        "form": form,
        }
                                                    ### Render the HTML template with the form
    return render(request, "categories/create.html", context)

##### http://localhost:8000/receipts/categories/create/


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():                         ### We should use the form to validate the values
            account = form.save(False)                                                       
            account.owner = request.user
            account.save()                          ### and save them to the DB
            return redirect("list_accounts")                 ### to another page and leave the function
    else:
        form  = CreateAccountForm()                ### Create an instance of the Django model form class
    context = {                                     ### Put the form in the context
        "form": form,
        }
                                                    ### Render the HTML template with the form
    return render(request, "accounts/create.html", context)

##### http://localhost:8000/receipts/accounts/create/


@login_required
def list_accounts(request):
    accounts_list = Account.objects.filter(owner=request.user)
    context = {
        "accounts_list": accounts_list, 
    }
    return render(request, "accounts/list.html", context)


@login_required
def list_categories(request):
    categories_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_list": categories_list, 
    }
    return render(request, "categories/list.html", context)